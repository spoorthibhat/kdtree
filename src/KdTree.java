import java.util.Stack;

import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdDraw;


public class KdTree {

	private Node root; // the root node of the tree.

	private int numOfPoints; // total number of points in the tree.

	// private Node inputNode;

	// private boolean basedOnX; // to help insert in right order.

	private static class Node {
		private Point2D point;      // the point
		private RectHV rect;    // the axis-aligned rectangle corresponding to this node
		private Node left;        // the left/bottom subtree
		private Node right;        // the right/top subtree
		private boolean orientationXVert;
		Node(Point2D point){
			this.point = point;

		}
	}

	/**
	 * Construct an empty set of points.
	 */
	public KdTree() {

		root = null;
		numOfPoints = 0;
		// inputNode = null;
		// basedOnX = true; // initially compare by x-coordinate.
	}

	/**
	 * Check if the set is empty.
	 * @return true if empty, else false.
	 */
	public boolean isEmpty() {
		// is the set empty? 
		return root == null;
	}

	/**
	 * number of points in the set.
	 * @return the size of the set.
	 */
	public int size() {
		// number of points in the set 
		return numOfPoints;
	}

	/**
	 * add the point to the set (if it is not already in the set)
	 * @param p - Point to be inserted.
	 */
	public void insert(Point2D p) {
		// add the point to the set (if it is not already in the set)
		if (!contains(p)) {
			Node inputNode = new Node(p);
			inputNode.orientationXVert = true; // initially
			insertIntoTree(inputNode, root);
			numOfPoints++;
		}
	}

	/**
	 * Helper method for insert().
	 * @param toBeInserted node to be inserted.
	 * @param current - current node.
	 */
	private void insertIntoTree(Node toBeInserted, Node current) {
		if (isEmpty()) {
			root = toBeInserted;
			root.rect = new RectHV(0, 0, 1, 1);
		} else if (current.orientationXVert) {
			if (toBeInserted.point.x() < current.point.x()) {
				if (current.left == null) {
					current.left = toBeInserted;
					// changing the orientation of child.
					current.left.orientationXVert = !current.orientationXVert; 
					current.left.rect = new RectHV(current.rect.xmin(), current.rect.ymin(), current.point.x(), current.rect.ymax());
				} else {
					insertIntoTree(toBeInserted, current.left);
				}
			} else {
				if (current.right == null) {
					current.right = toBeInserted;
					current.right.orientationXVert = !current.orientationXVert;
					current.right.rect = new RectHV(current.point.x(), current.rect.ymin(), current.rect.xmax(), current.rect.ymax());
				} else {
					insertIntoTree(toBeInserted, current.right);
				}
			} 
		} else if (!current.orientationXVert) { // horizontal orientation.
			if (toBeInserted.point.y() < current.point.y()) {
				if (current.left == null) {
					current.left = toBeInserted;
					current.left.orientationXVert = !current.orientationXVert;
					current.left.rect = new RectHV(current.rect.xmin(), current.rect.ymin(), current.rect.xmax(), current.point.y());
				} else {
					insertIntoTree(toBeInserted, current.left);
				}
			} else {
				if (current.right == null) {
					current.right = toBeInserted;
					current.right.orientationXVert = !current.orientationXVert;
					current.right.rect = new RectHV(current.rect.xmin(), current.point.y(), current.rect.xmax(), current.rect.ymax());
				} else {
					insertIntoTree(toBeInserted, current.right);
				}
			}
		}
	}

	/**
	 * does the set contain point p? 
	 * @param p - Point to be searched for.
	 * @return true if set contains p, else return false.
	 */
	public boolean contains(Point2D p) {
		// does the set contain point p? 
		if (!isEmpty()) {
			return search(p, root);
		}
		return false;
	}

	/**
	 * A helper private method for contains method to search for the given point.
	 * @param searchPoint the point being searched.
	 * @param current current node.
	 * @return true if set contains searchPoint, else return false.
	 */
	private boolean search(Point2D searchPoint, Node current) {

		if (current.orientationXVert) {
			if (current.left != null && searchPoint.x() < current.point.x()) {
				if (searchPoint.compareTo(current.left.point) == 0) {
					return true;
				} else {
					search(searchPoint, current.left);
				}
			} else if (current.right != null && searchPoint.x() >= current.point.x()) {
				if (searchPoint.compareTo(current.right.point) == 0) {
					return true;
				} else {
					search(searchPoint, current.right);
				}
			}
		} else {
			if (current.left != null && searchPoint.y() < current.point.y()) {
				if (searchPoint.compareTo(current.left.point) == 0) {
					return true;
				} else {
					search(searchPoint, current.left);
				}
			} else if (current.right != null && searchPoint.y() >= current.point.y()) {
				if (searchPoint.compareTo(current.right.point) == 0) {
					return true;
				} else {
					search(searchPoint, current.right);
				}
			}
		}

		return false;
	}

	/**
	 * all points that are inside the rectangle (or on the boundary)
	 * @param rect rectangle in which the points should be returned.
	 * @return the points in the rectangle.
	 */
	public void draw() {
		// draw all points to standard draw 
		// take x cordinate into consideration for vertical lines while drawing
		// and y coordinate while drawing horizontal
		// Use StdDraw.setPenColor(StdDraw.BLACK) and StdDraw.setPenRadius(0.01) before before drawing the points; 
		//use StdDraw.setPenColor(StdDraw.RED) or StdDraw.setPenColor(StdDraw.BLUE) and StdDraw.setPenRadius() 
		// before drawing the splitting lines.
		StdDraw.rectangle(0.5, 0.5, 0.5, 0.5);
		drawTree(root);

	}

	/**
	 * Helper method for draw().
	 * @param current the current node.
	 */
	private void drawTree(Node current) {

		if (current != null) {
			if (current.orientationXVert) {
				StdDraw.setPenColor(StdDraw.RED);
				StdDraw.setPenRadius();
				StdDraw.line(current.point.x(), current.rect.ymin(), current.point.x(), current.rect.ymax());
				StdDraw.setPenColor(StdDraw.BLACK);
				StdDraw.setPenRadius(0.01);
				StdDraw.point(current.point.x(), current.point.y());

			} else {
				StdDraw.setPenColor(StdDraw.BLUE);
				StdDraw.setPenRadius();
				StdDraw.line(current.rect.xmin(), current.point.y(), current.rect.xmax(), current.point.y());
				StdDraw.setPenColor(StdDraw.BLACK);
				StdDraw.setPenRadius(0.01);
				StdDraw.point(current.point.x(), current.point.y());
			}

			drawTree(current.left);
			drawTree(current.right);
		}
	}

	/**
	 * all points that are inside the rectangle (or on the boundary)
	 * @param rect rectangle in which the points should be returned.
	 * @return the points in the rectangle.
	 */
	public Iterable<Point2D> range(RectHV rect) {
		
		Stack<Point2D> stackOfPoints = new Stack<Point2D>();
		return rangeHelper(rect, stackOfPoints, root);
		
	}

	private Iterable<Point2D> rangeHelper(RectHV queryRect, Stack<Point2D> inputStack, Node current) {

		if (queryRect.contains(current.point)) {
			inputStack.push(current.point);
		}

			if (current.left != null && queryRect.intersects(current.left.rect)) {
				rangeHelper(queryRect, inputStack, current.left);
			}
			if (current.right != null && queryRect.intersects(current.right.rect)) {
				rangeHelper(queryRect, inputStack, current.right);
			}
		

		return inputStack;

	}
	public Point2D nearest(Point2D p)  {
		// a nearest neighbor in the set to point p; null if the set is empty 
		// double previousDistance = Double.MAX_VALUE;
		// Point2D closestPoint = null;
		return nearestHelper(p, root , root.point);

	}

	private Point2D nearestHelper(Point2D p, Node current, Point2D closest) {
		double currentDistance = p.distanceSquaredTo(current.point);
		if (currentDistance < p.distanceSquaredTo(closest)) {
			closest = current.point;
		}
		if (current.left != null && current.left.rect.contains(p)) {
			if (closest.distanceSquaredTo(p) >= current.left.rect.distanceSquaredTo(p)) {
				closest = nearestHelper(p, current.left, closest);
			}
			if (current.right != null && closest.distanceSquaredTo(p) >= current.right.rect.distanceSquaredTo(p)){
				closest = nearestHelper(p, current.right, closest);
			}
		} else if (current.right != null && current.right.rect.contains(p)) {
			if (closest.distanceSquaredTo(p) >= current.right.rect.distanceSquaredTo(p)){
				closest = nearestHelper(p, current.right, closest);
			}
			if (current.left != null && closest.distanceSquaredTo(p) >= current.left.rect.distanceSquaredTo(p)) {
				closest = nearestHelper(p, current.left, closest);
			}
		}
		
		/*if (current.left != null && current.right != null) {
			if (current.left.rect.contains(p)){
				if (currentDistance >= current.left.rect.distanceSquaredTo(p)) {
					nearestHelper(p, current.left, currentDistance, closest);
				}else if (currentDistance >= current.right.rect.distanceSquaredTo(p)) {
					nearestHelper(p, current.right, currentDistance, closest);
				}
				} else if (current.right.rect.contains(p)){ 
					if (currentDistance >= current.right.rect.distanceSquaredTo(p)) {
						nearestHelper(p, current.right, currentDistance, closest);
					}else if (currentDistance >= current.left.rect.distanceSquaredTo(p)) {
						nearestHelper(p, current.left, currentDistance, closest);
					}
				}
			}*/



		
		return closest;
	}





}
