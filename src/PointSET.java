import java.util.Stack;
import java.util.TreeSet;

import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdDraw;

public class PointSET {

	private TreeSet<Point2D> pointsTree; // set of points in tree

	/**
	 * Construct an empty set of points.
	 */
	public PointSET() {

		pointsTree = new TreeSet<Point2D>();
	}

	/**
	 * Check if the set is empty.
	 * @return true if empty, else false.
	 */
	public  boolean isEmpty() {
		// is the set empty? 
		return pointsTree.isEmpty();
	}

	/**
	 * number of points in the set.
	 * @return the size of the set.
	 */
	public int size()  {

		return pointsTree.size();
	}

	/**
	 * add the point to the set (if it is not already in the set)
	 * @param p - Point to be inserted.
	 */
	public void insert(Point2D p) {
		// 
		if (!pointsTree.contains(p)) {
			pointsTree.add(p);
		}
	}

	/**
	 * does the set contain point p? 
	 * @param p - Point to be searched for.
	 * @return true if set contains p, else return false.
	 */
	public boolean contains(Point2D p) {

		return pointsTree.contains(p);
	}

	/**
	 * draw all points to standard draw.
	 */
	public void draw() {

		for (Point2D i: pointsTree) {
			StdDraw.point(i.x(), i.y());
		}
	}

	/**
	 * all points that are inside the rectangle (or on the boundary)
	 * @param rect rectangle in which the points should be returned.
	 * @return the points in the rectangle.
	 */
	public Iterable<Point2D> range(RectHV rect) {

		Stack<Point2D> pointsInsideRect = new Stack<Point2D>();

		for (Point2D i: pointsTree) {
			if (rect.contains(i)) {
				pointsInsideRect.push(i);
			}
		}
		return pointsInsideRect;
	}

	/**
	 * a nearest neighbor in the set to point p; null if the set is empty
	 * @param p - Point to be compared with.
	 * @return - nearest point.
	 */
	public Point2D nearest(Point2D p) {

		Point2D nearest = null;
		double prevDistance = Double.MAX_VALUE;
		for ( Point2D i : pointsTree) {
			double distance = p.distanceSquaredTo(i);
			if ( distance < prevDistance) {
				nearest = i;
				prevDistance = distance ; 
			}
		}
		return nearest;
	}

	public static void main(String[] args) {
		// unit testing of the methods (optional) 
	}
}